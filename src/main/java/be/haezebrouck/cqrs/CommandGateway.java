package be.haezebrouck.cqrs;

public interface CommandGateway {

    void send(Object command);
}
