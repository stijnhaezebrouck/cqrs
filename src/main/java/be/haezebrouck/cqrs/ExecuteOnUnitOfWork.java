package be.haezebrouck.cqrs;

import org.axonframework.domain.AggregateRoot;
import org.axonframework.domain.EventMessage;
import org.axonframework.unitofwork.UnitOfWork;
import org.axonframework.unitofwork.UnitOfWorkListener;

import java.util.List;
import java.util.Set;

public class ExecuteOnUnitOfWork implements UnitOfWorkListener{

    private final Runnable afterCommit;

    public static ExecuteOnUnitOfWork afterCommit(Runnable execute) {
        return new ExecuteOnUnitOfWork(execute);

    }

    private ExecuteOnUnitOfWork(Runnable afterCommit) {
        this.afterCommit = afterCommit;
    }

    @Override
    public void afterCommit(UnitOfWork unitOfWork) {
        this.afterCommit.run();
    }

    @Override
    public void onRollback(UnitOfWork unitOfWork, Throwable failureCause) {

    }

    @Override
    public <T> EventMessage<T> onEventRegistered(UnitOfWork unitOfWork, EventMessage<T> event) {
        return event;
    }

    @Override
    public void onPrepareCommit(UnitOfWork unitOfWork, Set<AggregateRoot> aggregateRoots, List<EventMessage> events) {

    }

    @Override
    public void onPrepareTransactionCommit(UnitOfWork unitOfWork, Object transaction) {

    }

    @Override
    public void onCleanup(UnitOfWork unitOfWork) {

    }
}
