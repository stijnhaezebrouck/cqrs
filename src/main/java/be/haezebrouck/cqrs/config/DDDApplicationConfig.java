package be.haezebrouck.cqrs.config;

import be.haezebrouck.cqrs.CommandGateway;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.SimpleCommandBus;
import org.axonframework.commandhandling.gateway.CommandGatewayFactoryBean;
import org.axonframework.common.jdbc.ConnectionProvider;
import org.axonframework.common.jdbc.SpringDataSourceConnectionProvider;
import org.axonframework.common.jdbc.UnitOfWorkAwareConnectionProviderWrapper;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventhandling.EventTemplate;
import org.axonframework.eventhandling.SimpleEventBus;
import org.axonframework.eventhandling.annotation.AnnotationEventListenerBeanPostProcessor;
import org.axonframework.eventstore.EventStore;
import org.axonframework.eventstore.jdbc.*;
import org.axonframework.serializer.Serializer;
import org.axonframework.serializer.json.JacksonSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class DDDApplicationConfig {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private DataSource dataSource;

    @Bean
    public SpringClasspathScanner classpathScanner() {
        return new SpringClasspathScanner("be.haezebrouck.triptrap.foto");
    }

    @Bean
    public CommandBus commandBus() {
        return new SimpleCommandBus();
    }

    @Bean
    public EventBus eventBus() {
        return new SimpleEventBus();
    }

    @Bean
    public CommandGatewayFactoryBean<CommandGateway> commandGateway() {
        CommandGatewayFactoryBean<CommandGateway> factory = new CommandGatewayFactoryBean();
        factory.setCommandBus(commandBus());
        factory.setGatewayInterface(CommandGateway.class);
        return factory;
    }

    @Bean
    public EventTemplate eventTemplate() {
        return new EventTemplate(eventBus());
    }

    @Bean
    public EventSqlSchema eventSqlSchema() {
        return new be.haezebrouck.cqrs.db.MysqlEventSqlSchema<>(
                String.class,
                new SchemaConfiguration("axon_domain_event","axon_snapshot_event"));
    }

    @Bean
    public UnitOfWorkAwareConnectionProviderWrapper unitOfWorkAwareConnectionProvider() {
        ConnectionProvider connectionProvider =  new SpringDataSourceConnectionProvider(dataSource);
        return new UnitOfWorkAwareConnectionProviderWrapper(connectionProvider);
    }

    @Bean
    public EventEntryStore eventEntryStore() {
        return new DefaultEventEntryStore(
                //unitOfWorkAwareConnectionProvider(),
                new SpringDataSourceConnectionProvider(dataSource),
                eventSqlSchema());
    }

    @Bean
    public Serializer serializer() {
        return new JacksonSerializer(objectMapper);
    }

    @Bean
    public EventStore eventStore() {
        return new JdbcEventStore(eventEntryStore(), serializer());
    }


    @Bean
    public AnnotationEventListenerBeanPostProcessor annotationEventListenerBeanPostProcessor() {
        return new AnnotationEventListenerBeanPostProcessor();
    }

}
