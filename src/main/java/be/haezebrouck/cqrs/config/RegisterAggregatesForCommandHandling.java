package be.haezebrouck.cqrs.config;

import org.axonframework.commandhandling.CommandBus;
import org.axonframework.commandhandling.annotation.AggregateAnnotationCommandHandler;
import org.axonframework.eventhandling.EventBus;
import org.axonframework.eventsourcing.EventSourcedAggregateRoot;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.annotation.AbstractAnnotatedAggregateRoot;
import org.axonframework.eventstore.EventStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class RegisterAggregatesForCommandHandling {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private SpringClasspathScanner classpathScanner;

    @Autowired
    private CommandBus commandBus;

    @Autowired
    private EventBus eventBus;

    @Autowired
    private EventStore eventStore;

    @PostConstruct
    public void registerAggregateRoots() {

        for(Class<? extends EventSourcedAggregateRoot> aggregateRootClass :
                classpathScanner.scanForSubClasses(AbstractAnnotatedAggregateRoot.class)) {

            AggregateAnnotationCommandHandler.subscribe
                    (aggregateRootClass,
                            createEventSourcingRepository(aggregateRootClass),
                            commandBus);
            log.debug("Registered " + aggregateRootClass.getSimpleName() + " for command handling and event sourcing");
        }

    }

    private <T extends EventSourcedAggregateRoot> EventSourcingRepository createEventSourcingRepository(Class<T> aClass) {
        EventSourcingRepository<T> repository = new EventSourcingRepository(aClass, eventStore);
        repository.setEventBus(eventBus);
        return repository;
    }
}
