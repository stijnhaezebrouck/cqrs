package be.haezebrouck.cqrs.config;

import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AssignableTypeFilter;

import java.util.Set;
import java.util.stream.Collectors;

public class SpringClasspathScanner {

    private final String basePackage;

    public SpringClasspathScanner(String basePackage) {
        this.basePackage = basePackage;
    }

    private static final boolean NO_DEFAULT_FILTERS = false;

    public <S> Set<Class<? extends S>> scanForSubClasses(Class<S> superclass) {
        ClassPathScanningCandidateComponentProvider provider =
                new ClassPathScanningCandidateComponentProvider(NO_DEFAULT_FILTERS);
        provider.addIncludeFilter(new AssignableTypeFilter(superclass));
        Set classes = provider.findCandidateComponents(basePackage)
                .stream()
                .map(beanDef -> {
                    String className = beanDef.getBeanClassName();
                    return forName(className);
                })
                .collect(Collectors.toSet());
        return classes;
    }

    private Class<?> forName(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
}
