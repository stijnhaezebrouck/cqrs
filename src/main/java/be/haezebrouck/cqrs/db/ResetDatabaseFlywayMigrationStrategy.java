package be.haezebrouck.cqrs.db;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.stereotype.Service;

@Service
public class ResetDatabaseFlywayMigrationStrategy implements FlywayMigrationStrategy {

    @Value("${flyway.clean:false}")
    private boolean flywayClean;

    @Override
    public void migrate(Flyway flyway) {
        if (flywayClean) {
            flyway.clean();
        }
        flyway.migrate();
    }
}
