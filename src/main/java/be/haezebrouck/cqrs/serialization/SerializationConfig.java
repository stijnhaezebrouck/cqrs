package be.haezebrouck.cqrs.serialization;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;

@Configuration
public class SerializationConfig {
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(tripTrapModule());
        return objectMapper;
    }

    private Module tripTrapModule() {
        SimpleModule module = new SimpleModule("triptrapModule", new Version(0,0,0,null,null,null));
        module.addSerializer(Path.class, new PathSerializer());
        return module;
    }
}
