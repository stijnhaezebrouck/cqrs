package be.haezebrouck.cqrs.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;

import static java.util.concurrent.Executors.newSingleThreadExecutor;
import static java.util.concurrent.TimeUnit.SECONDS;


public class HashcodeOrderExecutor implements Executor {

    private Logger log = LoggerFactory.getLogger(getClass());

    private final List<ExecutorService> executors;

    public HashcodeOrderExecutor(int numberOfThreads, ThreadFactory threadFactory) {
        if (! (numberOfThreads > 0)) throw new IllegalArgumentException("numberOfThreads must be strictly positive");
        this.executors = new ArrayList<>(numberOfThreads);
        for (int i = 0;i<numberOfThreads;i++) {
            executors.add(newSingleThreadExecutor(threadFactory));
        }
    }

    private boolean active = true;


    public synchronized void shutdownUntilTerminated() {
        if (! active) return;
        active = false;
        executors.forEach(exec->exec.shutdown());
        executors.forEach(exec -> {
            try {
                exec.awaitTermination(30, SECONDS);
            } catch (InterruptedException e) {
                log.error(e.getMessage(), e);
            }
        });
    }

    @Override
    public synchronized void execute(Runnable runnable) {
        if (! active) throw new IllegalStateException("executor has been shutdown");
        executors.get(bucket(runnable)).execute(runnable);
    }

    private int bucket(Object object) {
        return Math.abs(object.hashCode()) % executors.size();
    }
}
