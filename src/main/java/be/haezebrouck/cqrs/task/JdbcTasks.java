package be.haezebrouck.cqrs.task;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.axonframework.eventhandling.annotation.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;
import be.haezebrouck.cqrs.task.event.TaskFailedEvent;
import be.haezebrouck.cqrs.task.event.TaskSucceededEvent;

import javax.annotation.PreDestroy;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.springframework.beans.factory.config.AutowireCapableBeanFactory.AUTOWIRE_NO;
import static be.haezebrouck.cqrs.task.TaskStatus.*;


/**
 * Thread safe once started
 * Invariant: must own lock on scheduler before setting any status to READY_TO_RUN
 */
@Service
public class JdbcTasks implements Tasks {

    private Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private AutowireCapableBeanFactory spring;

    private HashcodeOrderExecutor executor;

    private Scheduler scheduler = null;

    @EventListener
    public void notifyApplicationStarted(ContextRefreshedEvent event) {
        start();
    }

    public synchronized void start() {
        if (scheduler != null) log.warn("JdbcTasks already started");
        scheduler = new Scheduler();
        executor = new HashcodeOrderExecutor(10, new NamedThreadFactory("be/haezebrouck/cqrs/task"));
        resume();
        scheduler.start();
        log.info(getClass().getSimpleName() + " started");
    }

    @PreDestroy
    public synchronized void stop() throws InterruptedException {
        if (scheduler == null) return;
        scheduler.shutdown();
        executor.shutdownUntilTerminated();
        executor = null;
        scheduler = null;
        log.info(getClass().getSimpleName() + " has been stopped");
    }

    @EventHandler
    private void running(be.haezebrouck.cqrs.task.event.TaskRunningEvent event) {
        update(event.getId(), RUNNING);
    }

    @EventHandler
    private void success(TaskSucceededEvent event) {
        update(event.getId(), DONE);
    }

    @EventHandler
    private void failed(TaskFailedEvent event) {
        jdbcTemplate.update(
                "update task set status=?, error=? where id=?",
                FAILED.toString(),
                ExceptionUtils.getStackTrace(event.cause()),
                event.getId());
    }

    @Override
    public TaskStatus status(String id) {
        return TaskStatus.fromString(
                jdbcTemplate.queryForObject(
                        "select status from task where id=?",
                        String.class,
                        id));
    }

    private void update(String id, TaskStatus status) {
        jdbcTemplate.update(
                "UPDATE task SET status=? WHERE id=?",
                status.toString().toUpperCase(),
                id);
    }

    private void resume() {
        jdbcTemplate.update(
                "update task set status=? WHERE status=? OR status=?",
                READY_TO_RUN.toString(),
                SCHEDULED.toString(),
                RUNNING.toString()
        );
    }

    @Override
    public String add(Task task) {
        return addWithStatus(task, CREATED);
    }

    @Override
    public String addAndStart(Task task) {
        synchronized (scheduler) {
            String id=addWithStatus(task, READY_TO_RUN);
            log.debug("addAndStart task " + task.getId());
            scheduler.notify();
            log.debug("scheduler triggered");
            return id;
        }
    }

    private String addWithStatus(Task task, TaskStatus initialStatus) {
        log.debug("add task ({}), {}",task.getId(), initialStatus.toString());
        try {
            jdbcTemplate.update("insert into task (id,correlationId,taskType,status,serializedTask,description)" +
                            " values(?,?,?,?,?,?) on duplicate key update correlationId=?",
                    task.getId(),
                    task.getCorrelation(),
                    task.getClass().getName(),
                    initialStatus.toString(),
                    objectMapper.writeValueAsString(task),
                    task.description(),
                    task.getCorrelation());
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return task.getId();
    }

    public Task get(String id) {
        return jdbcTemplate.queryForObject(
                "select taskType, serializedTask from task where id=?",
                new SerializationRowMapper(),
                id);
    }

    private void resolveBeans(Task task) {
        spring.autowireBeanProperties(task, AUTOWIRE_NO, true);
    }

    @Override
    public void start(String id) {
        synchronized (scheduler) {
            update(id, READY_TO_RUN);
            log.debug("notify scheduler");
            scheduler.notify();
        }
    }

    private int scheduleTasks() {
        log.debug("schedule tasks");
        List<Task> tasks = jdbcTemplate.query("select taskType, serializedTask from task where status=?",
                new SerializationRowMapper(),
                READY_TO_RUN.toString());
        tasks.stream()
                .forEach(t-> {
                    update(t.getId(), SCHEDULED);
                    log.debug("add task to scheduler " + t.getId());
                    executor.execute(t);
                });
        return tasks.size();
    }

    public class SerializationRowMapper implements RowMapper<Task> {
        @Override
        public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
            String typeString = rs.getString("taskType");
            try {
                Class<?> type = Class.forName(typeString);
                String serializedTask = rs.getString("serializedTask");
                Task task = (Task) JdbcTasks.this.objectMapper.readValue(serializedTask, type);
                resolveBeans(task);
                return task;
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    private class Scheduler extends Thread {
        private boolean shutdown = false;

        private void shutdown() throws InterruptedException {
            shutdown = true;
            synchronized (this) { scheduler.notify(); }
            log.debug("waiting for scheduler shutdown...");
            join();
            log.debug("scheduler finished");
        }

        private Scheduler() {
            setName("Tasks.scheduler");
        }

        @Override
        public void run() {
            synchronized (this) {
                while (! shutdown) {
                    try {
                        int nb = scheduleTasks();
                        log.debug("done scheduling {} tasks, waiting...", nb);
                        wait();
                        log.debug("wakeup");
                    } catch (InterruptedException exc) {
                        shutdown = true;
                        log.debug("scheduler is interrupted for shutdown");
                    } catch (RuntimeException e) {
                        log.error(e.getMessage(), e);
                    }
                }
                log.debug("scheduler completed");
            }
        }
    }
}
