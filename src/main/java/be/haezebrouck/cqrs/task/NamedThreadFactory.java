package be.haezebrouck.cqrs.task;

import java.util.concurrent.ThreadFactory;

public class NamedThreadFactory implements ThreadFactory {

    private final String threadNamePrefix;

    public NamedThreadFactory(String threadNamePrefix) {
        this.threadNamePrefix = threadNamePrefix;
    }

    private int threadIndex = 1;

    @Override
    public Thread newThread(Runnable r) {
        String threadName = threadNamePrefix + "-" + threadIndex++;
        return new Thread(r, threadName);
    }
}
