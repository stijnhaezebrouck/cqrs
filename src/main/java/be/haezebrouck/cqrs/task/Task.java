package be.haezebrouck.cqrs.task;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.axonframework.eventhandling.EventTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import be.haezebrouck.cqrs.task.event.TaskFailedEvent;
import be.haezebrouck.cqrs.task.event.TaskRunningEvent;
import be.haezebrouck.cqrs.task.event.TaskSucceededEvent;

public abstract class Task implements Runnable {

    private final String id;
    private final String correlation;

    @Autowired
    @JsonIgnore
    private EventTemplate eventTemplate;

    protected Task(String id, String correlation) {
        this.id = id;
        this.correlation = correlation;
    }

    public final String getId() {
        return id;
    }

    public final String getCorrelation() { return correlation; }

    public abstract String description();

    @Override
    public final void run() {
        eventTemplate.publishEvent(new TaskRunningEvent(id, correlation, getClass().getName()));
        try {
            doRun();
            eventTemplate.publishEvent(new TaskSucceededEvent(id, correlation, getClass().getName()));
        } catch(Exception exc) {
            eventTemplate.publishEvent(new TaskFailedEvent(id, correlation, getClass().getName(), exc));
        }
    }

    @Override
    public int hashCode() {
        return (correlation == null ? id.hashCode() : correlation.hashCode());
    }

    protected abstract void doRun() throws Exception;

}
