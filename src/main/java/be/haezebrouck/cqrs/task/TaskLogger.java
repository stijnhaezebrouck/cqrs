package be.haezebrouck.cqrs.task;

import org.axonframework.eventhandling.annotation.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import be.haezebrouck.cqrs.task.event.TaskEvent;
import be.haezebrouck.cqrs.task.event.TaskFailedEvent;
import be.haezebrouck.cqrs.task.event.TaskRunningEvent;
import be.haezebrouck.cqrs.task.event.TaskSucceededEvent;

@Repository
public class TaskLogger {

    @EventHandler
    public void running(TaskRunningEvent event) {
        log(event, TaskStatus.RUNNING);
    }

    @EventHandler
    public void success(TaskSucceededEvent event) {
        log(event, TaskStatus.DONE);
    }

    @EventHandler
    public void failed(TaskFailedEvent event) {
        getLoggerFor(event).error("Task " + event.getId() + " "+ TaskStatus.FAILED, event.cause());
    }

    private void log(TaskEvent event, TaskStatus status) {
        getLoggerFor(event).debug("Task {} {}", event.getId(), status);
    }

    private Logger getLoggerFor(TaskEvent event) {
        return LoggerFactory.getLogger(getClass().getName() + "." + event.taskType());
    }

}
