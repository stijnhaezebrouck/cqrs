package be.haezebrouck.cqrs.task;

public enum TaskStatus {
    CREATED, READY_TO_RUN, RUNNING, RETRY, DONE, FAILED, SCHEDULED, PARKED;

    public static TaskStatus fromString(String stateString) {
        for (TaskStatus state:values()) {
            if (state.toString().equalsIgnoreCase(stateString)) return state;
        }
        throw new IllegalArgumentException("invalid state: " + stateString);
    }
}
