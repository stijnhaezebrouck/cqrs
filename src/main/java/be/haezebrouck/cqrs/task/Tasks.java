package be.haezebrouck.cqrs.task;

public interface Tasks {
    TaskStatus status(String id);

    /**
     * Adds a task, but do not schedule yet for execution.
     *
     * @param task the given task to add
     * @return the task id
     */
    String add(Task task);

    /**
     * Adds a task that is immediately available for execution (atomic operation)
     * @param task the given task to add and execute
     * @return the task id
     */
    String addAndStart(Task task);

    /**
     * Registers the task for execution.
     * @param id the id of the task that is already added
     */
    void start(String id);
}
