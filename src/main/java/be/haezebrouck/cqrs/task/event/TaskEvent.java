package be.haezebrouck.cqrs.task.event;

import java.util.Objects;

public abstract class TaskEvent {
    private final String id;
    private final String correlation;
    private final String taskType;

    TaskEvent(String id, String correlation, String taskType) {
        this.id = id;
        this.correlation = correlation;
        this.taskType = taskType;
    }

    public String getId() {
        return id;
    }

    public String getCorrelation() { return correlation; }

    public String taskType() {
        return taskType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TaskEvent)) return false;
        TaskEvent taskEvent = (TaskEvent) o;
        return Objects.equals(id, taskEvent.id) &&
                Objects.equals(taskType, taskEvent.taskType) &&
                Objects.equals(correlation, taskEvent.correlation) &&
                this.getClass().equals(taskEvent.getClass());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, taskType, correlation, getClass());
    }
}
