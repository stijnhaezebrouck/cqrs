package be.haezebrouck.cqrs.task.event;

public final class TaskFailedEvent extends TaskEvent {
    private final Throwable cause;

    public TaskFailedEvent(
            String id,
            String correlation,
            String taskType,
            Throwable cause) {
        super(id, correlation, taskType);
        this.cause = cause;
    }

    public Throwable cause() {
        return cause;
    }
}
