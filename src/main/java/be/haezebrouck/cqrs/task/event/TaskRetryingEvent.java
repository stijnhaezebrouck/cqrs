package be.haezebrouck.cqrs.task.event;

public class TaskRetryingEvent extends TaskEvent {
    public TaskRetryingEvent(String id, String taskType) {
        super(id, null,taskType);
    }
}
