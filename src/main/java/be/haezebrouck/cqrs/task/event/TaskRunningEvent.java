package be.haezebrouck.cqrs.task.event;

public class TaskRunningEvent extends TaskEvent {
    public TaskRunningEvent(String id, String correlation, String taskType) {
        super(id, correlation, taskType);
    }
}
