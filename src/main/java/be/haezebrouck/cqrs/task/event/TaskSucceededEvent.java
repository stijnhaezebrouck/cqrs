package be.haezebrouck.cqrs.task.event;

public final class TaskSucceededEvent extends TaskEvent {
    public TaskSucceededEvent(String id, String correlation, String taskType) {
        super(id, correlation, taskType);
    }
}
