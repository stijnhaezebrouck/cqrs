package be.haezebrouck.cqrs.task;

import org.junit.After;
import org.junit.Test;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.waitAtMost;

public class BucketExecutorServiceTest {

    private HashcodeOrderExecutor executorService = new HashcodeOrderExecutor(2, new NamedThreadFactory("test"));

    @After
    public void shutdownExecutorService() throws InterruptedException {
        executorService.shutdownUntilTerminated();
    }

    @Test
    public void whenHashcodeIsTheSame_shouldBeExecutedInOrder() throws InterruptedException {
        TestRunner runner1 = new TestRunner(1, true);
        TestRunner runner2 =  new TestRunner(1, false);

        executorService.execute(runner1);
        executorService.execute(runner2);


        waitAtMost(1,SECONDS).until(()->runner1.isWaiting());

        Thread.sleep(2000);
        //runner1 should block runner2
        assertThat(runner2.isFinished()).isFalse();

        synchronized (runner1) { runner1.notify();}
        waitAtMost(1, SECONDS).until(()->runner2.isFinished());
        assertThat(runner2.isFinished()).isTrue();
    }

    @Test
    public void whenHashcodeIsDifferent_shouldBeExecutedInParallel() throws InterruptedException {
        TestRunner runner1 = new TestRunner(1, true);
        TestRunner runner2 =  new TestRunner(2, false);

        executorService.execute(runner1);
        executorService.execute(runner2);


        waitAtMost(1,SECONDS).until(()->runner1.isWaiting());
        waitAtMost(1,SECONDS).until(()->runner2.isFinished());

        synchronized (runner1) { runner1.notify();}
        waitAtMost(1, SECONDS).until(()->runner1.isFinished());
    }
}
