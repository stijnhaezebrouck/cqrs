package be.haezebrouck.cqrs.task;


import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@RunWith(SpringRunner.class)
@ContextConfiguration(classes=TestConfiguration.class)
@ActiveProfiles("test")
public abstract class SpringBootIntegrationTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Before
    public void cleanupDatabaseTables() {
        JdbcTestUtils.deleteFromTables(jdbcTemplate,
                "axon_domain_event",
                "axon_saga_association_value_entry",
                "axon_saga_entry",
                "axon_snapshot_event");
    }
}
