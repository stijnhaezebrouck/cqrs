package be.haezebrouck.cqrs.task;

import be.haezebrouck.cqrs.task.event.TaskFailedEvent;
import be.haezebrouck.cqrs.task.event.TaskRunningEvent;
import be.haezebrouck.cqrs.task.event.TaskSucceededEvent;
import org.axonframework.eventhandling.EventTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(MockitoJUnitRunner.class)
public class TaskTest {

    @Mock
    private EventTemplate eventTemplate;

    @Mock
    private TestTaskResource taskResource;

    @InjectMocks
    private TestTask testTaak = new TestTask("id","correlation");

    @Test
    public void taakStart_happyPath() {
        testTaak.run();

        InOrder order = Mockito.inOrder(eventTemplate, taskResource);
        order.verify(eventTemplate).publishEvent(new TaskRunningEvent("id", "correlation",TestTask.class.getName()));
        order.verify(taskResource).touch();
        order.verify(eventTemplate).publishEvent(new TaskSucceededEvent("id", "correlation", TestTask.class.getName()));
    }

    @Test
    public void taakStart_error() {
        Exception exc = new Exception("failed");
        testTaak.fail(exc);
        testTaak.run();

        InOrder order = Mockito.inOrder(eventTemplate, taskResource);
        order.verify(eventTemplate).publishEvent(new TaskRunningEvent("id", "correlation",TestTask.class.getName()));
        order.verify(taskResource).touch();
        order.verify(eventTemplate).publishEvent(new TaskFailedEvent("id", "correlation", TestTask.class.getName(), exc));
    }

    @Test
    public void hashCode_whenCorrelationIsSpecified() {
        assertThat(new TestTask("id","correlation").hashCode()).isEqualTo("correlation".hashCode());
    }

    @Test
    public void hashCode_whenCorrelationIdIsNotSpecified() {
        assertThat(new TestTask("id", null).hashCode()).isEqualTo("id".hashCode());
    }
}