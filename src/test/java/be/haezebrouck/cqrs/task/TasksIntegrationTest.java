package be.haezebrouck.cqrs.task;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.jdbc.JdbcTestUtils;

import static be.haezebrouck.cqrs.task.TaskStatus.*;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

public class TasksIntegrationTest extends SpringBootIntegrationTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private JdbcTasks tasks;

    @Autowired
    private TestTaskResource testTaskResource;

    @Before
    public void cleanupTasks() throws InterruptedException {
        tasks.stop();
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "task");
        tasks.start();
    }

    private int numberOfTasks() {
        return JdbcTestUtils.countRowsInTable(jdbcTemplate, "task");
    }

    @Test
    public void addTask_isAddedToTheQueue () {
        TestTask taak = new TestTask("id", "correlation");
        taak.setState("teststate");
        String id = tasks.add(taak);

        assertThat(tasks.status(id)).isEqualTo(CREATED);
        TestTask retrievedTask = (TestTask) tasks.get(id);
        assertThat(retrievedTask.getId()).isEqualTo(id);
        assertThat(retrievedTask.getState()).isEqualTo("teststate");
        assertThat(retrievedTask.getResource()).isSameAs(testTaskResource);
        assertThat(retrievedTask.getCorrelation()).isEqualTo("correlation");
    }

    @Test
    public void addTask_whenAlreadyAdded_onlyAddedOnce() {
        TestTask taak = new TestTask("id","correlation");
        tasks.add(taak);
        assertThat(numberOfTasks()).isEqualTo(1);
        tasks.add(taak);
        assertThat(numberOfTasks()).isEqualTo(1);
    }

    @Test
    public void addAndStart_getsExecuted() {
        testTaskResource.reset();
        tasks.addAndStart(new TestTask("id","correlation"));
        await().atMost(10,SECONDS).until(()->tasks.status("id")==DONE);
        assertThat(testTaskResource.isTouched()).isTrue();
    }

    @Test
    public void task_whenStarted_getsExecuted() {
        testTaskResource.reset();
        tasks.add(new TestTask("id","correlation"));
        tasks.start("id");
        await().atMost(30, SECONDS).until(()->tasks.status("id")== DONE);
        assertThat(testTaskResource.isTouched()).isTrue();
    }

    @Test
    @DirtiesContext
    public void tasks_whenresumed_executes_all_non_failed_tasks() throws InterruptedException {
        testTaskResource.reset();
        tasks.stop();
        addTask("created", CREATED);
        addTask("readyToRun", READY_TO_RUN);
        addTask("running", RUNNING);
        addTask("scheduled", SCHEDULED);
        addTask("done", DONE);
        addTask("failed", FAILED);
        tasks.start();

        await().atMost(1,SECONDS).until(()->tasks.status("readyToRun")==DONE);
        await().atMost(1,SECONDS).until(()->tasks.status("running")==DONE);
        await().atMost(1,SECONDS).until(()->tasks.status("scheduled")==DONE);

        Thread.sleep(5000);
        assertThat(tasks.status("created")==CREATED);
        assertThat(tasks.status("failed")==FAILED);
        assertThat(tasks.status("done")==DONE);
    }

    private void addTask(String id, TaskStatus status) {
        tasks.add(new TestTask(id, "correlation"));
        jdbcTemplate.update("update task set status=? where id=?",status.toString(), id);
    }
}