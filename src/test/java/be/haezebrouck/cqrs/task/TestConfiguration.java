package be.haezebrouck.cqrs.task;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration
@Configuration
@ComponentScan({"be.haezebrouck.cqrs"})
public class TestConfiguration {
}

