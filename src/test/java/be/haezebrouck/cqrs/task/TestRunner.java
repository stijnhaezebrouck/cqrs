package be.haezebrouck.cqrs.task;


public class TestRunner implements Runnable {

    private final int hashCode;
    private final boolean blocking;
    private boolean waiting = false;
    private boolean finished = false;

    public TestRunner(int hashCode, boolean blocking) {
        this.hashCode = hashCode;
        this.blocking = blocking;
    }

    public synchronized boolean isFinished() {
        return finished;
    }

    public synchronized boolean isWaiting() {
        return waiting;
    }

    @Override
    public int hashCode() {
        return hashCode;
    }

    @Override
    public synchronized void run() {
        try {
            if (blocking) {
                waiting = true;
                wait();
                waiting = false;
            }
            finished = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
