package be.haezebrouck.cqrs.task;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.beans.factory.annotation.Autowired;

public class TestTask extends Task {

    @JsonIgnore
    @Autowired
    private TestTaskResource resource;

    @JsonProperty("fail")
    private Exception fail;

    private String state = "someState";

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public TestTaskResource getResource() {
        return resource;
    }

    @JsonCreator
    public TestTask(
            @JsonProperty("id") String id,
            @JsonProperty("correlation") String correlation) {
        super(id, correlation);
    }

    @Override
    public String description() {
        return "";
    }

    @Override
    protected void doRun() throws Exception {
        resource.touch();
        if (fail!=null) throw fail;
    }

    public void fail(Exception exc) {
        fail = exc;
    }
}
