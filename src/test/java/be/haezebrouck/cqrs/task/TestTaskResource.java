package be.haezebrouck.cqrs.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class TestTaskResource {

    @Autowired
    private JdbcTemplate somthingThatDoesNotSerialize;


    private boolean touched = false;

    public boolean isTouched() {
        return touched;
    }

    public void touch() {
        this.touched = true;
    }

    public void reset() {
        this.touched = false;
    }
}
