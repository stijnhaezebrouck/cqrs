package be.haezebrouck.cqrs.task;

import java.util.UUID;

public class TestTaskTriggeringEvent {

    private final String description;
    private final long runtimeMs;
    private final boolean failing;
    private UUID uuid = UUID.randomUUID();

    private TestTaskTriggeringEvent(String description, long runtimeMs, boolean failOnce) {
        this.description = description;
        this.runtimeMs = runtimeMs;
        this.failing = failOnce;
    }

    public static TestTaskTriggeringEvent triggerSucceedingTask(String description, long runtimeMs) {
        return new TestTaskTriggeringEvent(description, runtimeMs, false);
    }

    public static TestTaskTriggeringEvent triggerSucceedingTaskAfterResume(String description, long runtimeMs) {
        return new TestTaskTriggeringEvent(description, runtimeMs, true);
    }

    public String getProperty() {
        return uuid.toString();
    }

    public String description() {
        return description;
    }

    public long runtimeMs() {
        return runtimeMs;
    }

    public boolean shouldFail() {
        return failing;
    }

}
