CREATE TABLE axon_domain_event (
  aggregateIdentifier VARCHAR(255) NOT NULL,
  sequenceNumber      BIGINT       NOT NULL,
  type                VARCHAR(255) NOT NULL,
  eventIdentifier     VARCHAR(255) NOT NULL,
  metaData            TEXT,
  payload             TEXT         NOT NULL,
  payloadRevision     VARCHAR(255),
  payloadType         VARCHAR(255) NOT NULL,
  timeStamp           TIMESTAMP(3) NOT NULL,
  PRIMARY KEY (aggregateIdentifier, sequenceNumber, type)
);

CREATE TABLE axon_snapshot_event (
  aggregateIdentifier VARCHAR(255) NOT NULL,
  sequenceNumber      BIGINT       NOT NULL,
  type                VARCHAR(255) NOT NULL,
  eventIdentifier     VARCHAR(255) NOT NULL,
  metaData            TEXT,
  payload             TEXT         NOT NULL,
  payloadRevision     VARCHAR(255),
  payloadType         VARCHAR(255) NOT NULL,
  timeStamp           TIMESTAMP(3) NOT NULL,
  PRIMARY KEY (aggregateIdentifier, sequenceNumber, type)
);
