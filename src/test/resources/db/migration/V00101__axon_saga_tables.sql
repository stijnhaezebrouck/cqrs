CREATE TABLE axon_saga_entry (
  sagaId         VARCHAR(255) NOT NULL,
  revision       VARCHAR(255),
  sagaType       VARCHAR(255),
  serializedSaga MEDIUMTEXT,
  PRIMARY KEY (sagaId),
  INDEX saga_type_idx (sagaType)
);

CREATE TABLE axon_saga_association_value_entry (
  id               INT NOT NULL AUTO_INCREMENT,
  associationKey   VARCHAR(255),
  associationValue VARCHAR(255),
  sagaId           VARCHAR(255),
  sagaType         VARCHAR(255),
  PRIMARY KEY (id),
  INDEX association_key_idx (associationKey),
  INDEX association_value_idx (associationKey),
  INDEX saga_type_idx (sagaType)
);