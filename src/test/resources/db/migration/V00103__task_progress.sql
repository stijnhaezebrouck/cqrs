CREATE TABLE task (
  creation_timestamp    TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_update_timestamp TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  id                    VARCHAR(255) NOT NULL,
  sagaId                VARCHAR(255) NOT NULL,
  taskType              VARCHAR(255) NOT NULL,
  description           VARCHAR(255) NOT NULL,
  status                VARCHAR(255) NOT NULL,
  error                 VARCHAR(2048),
  PRIMARY KEY (id),
  UNIQUE INDEX saga_id_uidx (sagaId),
  INDEX task_type_idx (taskType),
  INDEX task_status_idx (status)
);
