ALTER TABLE task
  DROP INDEX saga_id_uidx,
  CHANGE COLUMN sagaId correlationId VARCHAR(255),
  ADD INDEX correlation_id_idx(correlationId),
  ADD COLUMN serializedTask TEXT,
  MODIFY COLUMN description VARCHAR(255),
  MODIFY COLUMN error LONGTEXT;
